/**************************************************************************************/
/* Esta funcion se encarga de mostrar y agregarle funcionalidad a la barra de tareas de macOS.

 Recibe 3 parametros:
      struct nk_context *ctx-       Puntero necesario para el funcionamiento de Nuklear
      struct media *media           Puntero que controla y guarda todos los archivos multimedia cargados
                                    al iniciar el programa.
      struct programas *programas   Punterio a la estructura programas que guarda booleanas y controla
                                    que programas estan activados para mostrarse en la interfaz.

Retorna un entero:    Si es 1 entonces la ventana esta mostrandose
                        Si es 0 entonces esta escondida
************************************************************************************/
static int
barra_superior(struct nk_context *ctx,  struct media *media, struct programas *programas)
{
    /*Variable necesaria para calcular el tiempo*/
    time_t rawtime;
    struct tm *info;
    char buffer[80];

    time( &rawtime );

    info = localtime( &rawtime );

    /*Enviamos una string el tiempo con el formato deseado*/
    sprintf(buffer, "%02d : %02d : %02d", info->tm_hour, info->tm_min, info->tm_sec);
    /*
    Con nk_begin empezamos a pintar la ventana, en este caso la barra superior.
    Si la creacion es exitosa entonces retorna 1, de lo contrario retorna 0.

    Notese que el ancho es un macro declarado en main.c
    */
    if (nk_begin(ctx, "Titulo", nk_rect(0, 0, WINDOW_WIDTH, 20), NK_WINDOW_NO_SCROLLBAR))
    {

      //Empezamos a crear un menu en la barra de la ventana, que es con lo que interactuara el usuario
      nk_menubar_begin(ctx);
      //Empezamos una row nueva para empezar a meterle elementos
      //En este caso optamos por hacerla estatica porque deseamos meter el reloj de viaje a la derecha
      nk_layout_row_begin(ctx, NK_STATIC, 15, 6);
      //Hacemos un push con espacio de 15 para el primer elemento de la barra,
      //en este caso es el logo.
      nk_layout_row_push(ctx, 15);
      //Hacemos un nk_menu_begin_image que recibe varios parametros, entre ellos
      //El puntero a la imagen a cargar, contenido el punterio tipo media.
      if (nk_menu_begin_image(ctx,"" ,media->apple_logo,  nk_vec2(180, 500))) {
        //Aca vamos creando cada uno de los submenus.
        //Cada submenu tiene su imagen y su nombre y cada uno cuando se le da click
        //retorna un 1 a la funcion, esto significa que cuando se da click debe activar
        //una variable en la struct programas y pasarla a true o false, dependiendo de su estado anterior.
        //Para posteriormente en el while principar mostrar o no el programa en cuestion.
        nk_layout_row_dynamic(ctx, 20, 1);
        if (nk_menu_item_image_label(ctx, media->calculadora,"Calculadora", NK_TEXT_RIGHT)){
          programas->calculadora = programas->calculadora?nk_false:nk_true;
        }
        if (nk_menu_item_image_label(ctx, media->editor_texto,"Note Pad", NK_TEXT_RIGHT)){
          programas->editor = programas->editor?nk_false:nk_true;          
        }
        if (nk_menu_item_image_label(ctx, media->finder,"Finder", NK_TEXT_RIGHT)){
          programas->finder = programas->finder?nk_false:nk_true;
        }

        nk_menu_end(ctx);
      }

      nk_layout_row_push(ctx, 25);
    //   /*Menu File*/
    //   if (nk_menu_begin_label(ctx, "File", NK_TEXT_LEFT, nk_vec2(120, 200))) {
    //       nk_layout_row_dynamic(ctx, 30, 1);
    //       if (nk_menu_item_label(ctx, "Submenu 1", NK_TEXT_LEFT))
    //           fprintf(stdout, "Submenu 1\n");
    //       if (nk_menu_item_label(ctx, "Submenu 2", NK_TEXT_LEFT)){

    //       }
    //       if (nk_menu_item_label(ctx, "Submenu 2", NK_TEXT_LEFT))
    //           fprintf(stdout, "Submenu 2\n");

    //       nk_menu_end(ctx);
    //   }

    //   /*Menu Edit*/
    //   nk_layout_row_push(ctx, 25);
    //   if (nk_menu_begin_label(ctx, "Edit", NK_TEXT_LEFT, nk_vec2(120, 200))) {
    //       nk_layout_row_dynamic(ctx, 30, 1);
    //       if (nk_menu_item_label(ctx, "Submenu 1", NK_TEXT_LEFT))
    //           fprintf(stdout, "Submenu 1\n");
    //       if (nk_menu_item_label(ctx, "Submenu 2", NK_TEXT_LEFT)){

    //       }
    //       if (nk_menu_item_label(ctx, "Submenu 2", NK_TEXT_LEFT))
    //           fprintf(stdout, "Submenu 2\n");

    //       nk_menu_end(ctx);
    //   }
      /*Menu Help*/
      nk_layout_row_push(ctx, 40);
      if (nk_menu_begin_label(ctx, "Ayuda", NK_TEXT_LEFT, nk_vec2(120, 200))) {
          nk_layout_row_dynamic(ctx, 30, 1);
          if (nk_menu_item_label(ctx, "Informacion", NK_TEXT_LEFT))
            programas->about = programas->about?nk_false:nk_true;

          nk_menu_end(ctx);
      }

      /*********************/
      /*    Reloj         */
      /*******************/
      nk_layout_row_push(ctx, 900);
      nk_label(ctx,buffer,NK_TEXT_RIGHT);


    }
    nk_end(ctx);
    return !nk_window_is_closed(ctx, "Titulo");
}
