
static int
finder(struct nk_context *ctx, struct tinydir_dir *dir, struct iconos *iconos, char directorioFinder[PATH_MAX], char directorioEditor[PATH_MAX] ,struct stack_t *pilaDirectoriosFinder, int *ptrmostrandoPopup, struct programas *programas,  char ptrNombreArchivo[PATH_MAX] )
{
  /*
  Para que las opciones de minimizar y cerrar aparezcan en la ezquina superior izquierda
  de la ventana
  */
  const struct nk_input* inpt = &ctx->input;
  static enum nk_style_header_align header_align = NK_HEADER_LEFT;
  ctx->style.window.header.align = header_align;
    if (nk_begin(ctx, "Finder", nk_rect((WINDOW_WIDTH/2)-(800/2), (WINDOW_HEIGHT/2)-(500/2), 800, 500),
    NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_CLOSABLE|NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE|NK_WINDOW_SCALABLE))
    {

      static char text[50];
      //Antes de entrar al ciclo que muestra los archivos, necesitamos crear
      //una string del directorio al que queremos entrar.
      // Esto lo logamos recorriendo y revirtiendo en una pila temporal
      // la pila que actualmente estamos usando.       
      if (pilaDirectoriosFinder !=NULL ) {
        strcpy(directorioFinder,"./");
        //Necesitamos crear una stack temporal para revertir los valores
        struct stack_t *pilaTmp = nuevaStack();
        //Creamos un nuevo itemStack al cual le asignamos el tope de la pila de directorios
        struct itemStack *dirTemp = pilaDirectoriosFinder->tope;
        //Recorremos hacia atras la stack
        for (size_t i = 0; i <pilaDirectoriosFinder->tamStack; i++) {
          push(pilaTmp,dirTemp->dato);
          if (dirTemp->siguiente !=NULL) {
            dirTemp = dirTemp->siguiente;
          }
        }
        dirTemp = pilaTmp->tope;
        for (size_t i = 0; i < pilaTmp->tamStack; i++) {
          strcat(directorioFinder,dirTemp->dato);
          strcat(directorioFinder,"/");
          dirTemp = dirTemp->siguiente;
        }
        dirTemp = NULL;
        destructorPila(&pilaTmp);      
      }

      nk_layout_row_begin(ctx, NK_STATIC, 40, 4);
      nk_layout_row_push(ctx,40);
      //Vamos haciendo pop de la stack cuando se presiona el boton para atras
      if (nk_button_image(ctx,iconos->atrasFlecha)) {
        //Obtenemos de la pila el ultimo directorio
          pop(pilaDirectoriosFinder);
          //SI la pila esta vacía entonces ponemos "home" por defecto, de esta manera siempre quedaremos
          //en home.
        if (pilaDirectoriosFinder->tamStack==0) {
          push(pilaDirectoriosFinder,"home");
        }
      }
      nk_layout_row_push(ctx,40);
      if (nk_button_image(ctx,iconos->homeIcono)) {
        printf("Navegando hacia home\n");
        //QUitamos todos los items que tenga la pila
        limpiarStack(pilaDirectoriosFinder);
        //Y agregamos uno, que seria el dir HOME
        push(pilaDirectoriosFinder,"home");
        printf("Elemento tope de la stack: %s\n", tope(pilaDirectoriosFinder));
      }
      float ventanaDimen = nk_window_get_width(ctx);
      nk_layout_row_push(ctx,ventanaDimen-160);
      // nk_button_label(ctx,tope(pilaDirectoriosFinder));
      nk_button_label(ctx,directorioFinder);
      nk_layout_row_push(ctx,40);
      //EL boton de carpeta nueva controla si la booleana mostrandoPopup es activada      
      if (nk_button_image(ctx,iconos->nuevaCarpeta)) {
        *ptrmostrandoPopup = 1;
      }
      if (*ptrmostrandoPopup) {
        static struct nk_rect s = {(WINDOW_WIDTH/2)-(340/2), (WINDOW_HEIGHT/2)-(150/2), 340, 150};
        //Empezamos a mostrar el dialoguito de crear nueva carpeta
        if (nk_popup_begin(ctx, NK_POPUP_STATIC, "Crear nueva carpeta", 0, s)){
            /*Texto al lado izquierdo del textedit*/
            nk_layout_row_begin(ctx, NK_STATIC, 30, 3);
            nk_layout_row_push(ctx, 100);
            nk_label(ctx, "Nombre:", NK_TEXT_LEFT);

            /*Textedit para el nombre del archivo*/
            nk_layout_row_push(ctx, 150);
            // NK_API nk_flags nk_edit_string(struct nk_context*, nk_flags, char *buffer, int *len, int max, nk_plugin_filter);
            nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, &text[0], 20, nk_filter_default);
            nk_layout_row_end(ctx);

            nk_layout_row_dynamic(ctx, 25, 1);
            nk_layout_row_dynamic(ctx, 25, 2);
            //Logica a ejecutar si acepta 
            if (nk_button_label(ctx, "Aceptar")) {
                char tmporal[100];
                //primero le metemos la string del directorio actual
                strcpy(tmporal,directorioFinder);
                //luego concatenamos el nombre de la carpeta
                strcat(tmporal,text);
                //creamos con mkdir el directorio
                if (mkdir(tmporal,0777) == 0) {
                  printf("Carpeta creada: %s\n", tmporal);
                }
                *ptrmostrandoPopup = 0;
                nk_popup_close(ctx);
                //limpiamos el cuadrito de texto para que no salga al momento de volverlo a abrir
                strcpy(text,"");
            }
            if (nk_button_label(ctx, "Cancelar")) {
                *ptrmostrandoPopup = 0;
                nk_popup_close(ctx);
                strcpy(text,"");
            }

            nk_popup_end(ctx);
        }
      }

      //EMpezamos a crear el contenedor donde mostraremos la lista      
      nk_layout_row_end(ctx);
      //EL algo del contenedor va a ir controlado obteniendo el alto de la ventana
      //y asignandolo al row dynamic que es el que controla el grupo/container.
      float alto = nk_window_get_height(ctx)-100;      
      nk_layout_row_dynamic(ctx, (int)alto, 1);     
      nk_group_begin(ctx, "Archivos", NK_WINDOW_BORDER|NK_WINDOW_TITLE);
      {

        //Creamos un nuevo row dentro delgrupo nuevo, este row
        //tendra 30 de alto y tres elementos
        nk_layout_row_dynamic(ctx, 30, 3);
        //Abrimos el directorio, en este caso usamos tiny_dir
        if (tinydir_open(dir, directorioFinder) == -1) {
          //Manejo de errores
          printf("Fallo al abrir el directorioFinder %s\n", directorioFinder);
        }else{
          while (dir->has_next)
          {

            // cantArchivos++;
            tinydir_file file;
            if (tinydir_readfile(dir, &file) == -1)
            {
              perror("Error obteniendo el archivo");
            }

            if (file.is_dir)
            {
              //El archivo es un directorio
              // cantCarpetas++;
              if ( strcmp(file.name,".") !=0 )
                if (strcmp(file.name,"..")!=0){
                  //Debido a que el fichero es una carpeta, dibujamos un button image label con
                  //icono de carpeta.

                  if(nk_button_image_label(ctx,iconos->carpetaIcon,file.name,NK_TEXT_LEFT)){
                    //Al darle clic al icono de carpeta enviamos a la pila la nueva carpeta
                    //esto actualizaria la interfaz y movería el finder a la carpeta seleccionada
                    push(pilaDirectoriosFinder,file.name);
                    /**Debuggin
                    printf("[PILA]->La pila tiene %zu elementos\n", pilaDirectoriosFinder->tamStack);
                    struct itemStack *tmpItem = pilaDirectoriosFinder->tope;
                    for (size_t i = 0; i < pilaDirectoriosFinder->tamStack; i++) {
                      printf("%s\n", tmpItem->dato);
                      tmpItem = tmpItem->siguiente;
                    }
                    printf("Ultimo directorio ahora es-> %s\n", pilaDirectoriosFinder->tope->siguiente->dato);
                    printf("Cambiando a carpeta-> %s\n", tope(pilaDirectoriosFinder));
                    **/
                  }
                }
            }else{
              //Mostramos en pantalla
              // cantFicheros++;
              if(nk_button_image_label(ctx,iconos->textIcon,file.name,NK_TEXT_LEFT)){              
                printf("Abriendo->%s\n",file.path);
                // strcpy(ptrNombreArchivo, file.path);
              }

            }
            if (tinydir_next(dir) == -1)
            {
              perror("No pudimos obtener el siguiente archivo");
            }

          }
          
          tinydir_close(dir);
        }


      }nk_group_end(ctx);


    }
    nk_end(ctx);
    return !nk_window_is_hidden(ctx, "Finder");
}
