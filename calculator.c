// funcion de ejecucion, 
// parametros nk_context, puntero de reseteo de la pantalla de la calculadora
static int
calculator(struct nk_context *ctx, int *resetCalculadora)
{
  //Variables necesarias para el diseño de la ventana de la calculadora  
  static enum nk_style_header_align header_align = NK_HEADER_LEFT;
  ctx->style.window.header.align = header_align;
  // Creación de la ventana
  // condicional para verificar que todo se haya creado a la perfección
    if (nk_begin(ctx, "Calculadora", nk_rect((WINDOW_WIDTH/2), (WINDOW_HEIGHT/2), 180, 250),
    NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_CLOSABLE|NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
    {
        // variables de operacion al ser static mantienen sus valores en cada llamada
        // set  -> para saber que se ha ingresado un operando y pasar a otro
        // prev -> controlar la operacion anterior si fue +, -, *, ó /
        static int set = 0, prev = 0, op = 0;
        // arreglo de constantes para operadores y numeros del grid
        static const char numbers[] = "789456123";
        static const char ops[] = "+-*/";
        // variables para los operandos
        static double a = 0, b = 0;
        static double *current = &a;

        // comprobando si se ha reseteado la calculadora, es decir, que se este abriendo de nuevo
        if (*resetCalculadora) {
            printf("Reseteando el estado de la calculadora\n");
            a = b = op = 0; current = &a; set = 0;
        }
        //variable para el control de botones en el grid
        size_t i = 0;
        //variable que indica la ejecucion de una operacion
        int solve = 0;

        //variables de ambitos cortos, texto a mostrar, buffer, etc.
        {int len; char buffer[256];
        nk_layout_row_dynamic(ctx, 35, 1);
        len = snprintf(buffer, 256, "%.2f", *current);
        nk_edit_string(ctx, NK_EDIT_SIMPLE, buffer, &len, 255, nk_filter_float); //permite editar el texto con el valor en buffer
        buffer[len] = 0;
        *current = atof(buffer);}//traemos al puntero lo que esta en buffer} 

        //creando nuestro grid
        nk_layout_row_dynamic(ctx, 35, 4);
        //16 por los digitos en la grid
        for (i = 0; i < 16; ++i) {
            // se colocan los botones C, 0 y =
            if (i >= 12 && i < 15) {
                //se colocan una sola vez los botones inferiores, despues se procede a saltar el paso
                if (i > 12) continue;
                //crea y valida accion en el boton
                if (nk_button_label(ctx, "C")) {
                    a = b = op = 0; current = &a; set = 0;
                } if (nk_button_label(ctx, "0")) {
                    *current = *current*10.0f; set = 0;
                } if (nk_button_label(ctx, "=")) {
                    solve = 1; prev = op; op = 0;
                }
            // condicion para dibujar los numeros, el valor de i cuyo residuo por 4 sea 1 indica la posicion de los numeros
            // en el grid
            } else if (((i+1) % 4)) {
                //se coloca el numero y se evalua si reciben clic
                if (nk_button_text(ctx, &numbers[(i/4)*3+i%4], 1)) {
                    //se almacena el numero ingresado, la formula nos permite ingresar numeros mayores a 9
                    *current = *current * 10.0f + numbers[(i/4)*3+i%4] - '0';
                    set = 0;
                }
            // condicion para dibujar los operadores en el grid
            } else if (nk_button_text(ctx, &ops[i/4], 1)) {
                // se evalua que ha estado ingresando el primer operando
                if (!set) {
                    // se evalua si se estaba apuntando al segundo operando cuando se ingresaba, sino es asi se apunta a ese
                    if (current != &b) {
                        current = &b;
                    } else {
                        // caso que nos permite operaciones continuas, 
                        prev = op;
                        solve = 1;
                    }
                }
                // se guarda la operacion y bandera que indica que se ingresa el segundo operando
                op = ops[i/4];
                set = 1;
            }
        }
        // se evalua la operacion
        if (solve) {
            if (prev == '+') a = a + b;
            if (prev == '-') a = a - b;
            if (prev == '*') a = a * b;
            if (prev == '/') a = a / b;
            // se apunta al primer operando para mostrarlo en el buffer
            current = &a;
            // si se ha estado ingresando el segundo operando se apunta a este
            if (set) current = &b;
            // reseteo del segundo operando y bandera de ingreso de operandos
            b = 0; set = 0;
        }
    }
    nk_end(ctx); //se termina de crear la ventana
    return !nk_window_is_hidden(ctx, "Calculadora"); // retorna si ha sido cerrada o no la ventana de la calculadora
}
