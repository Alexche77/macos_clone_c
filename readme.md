# Clon básico de macOS 9
![Captura del software](https://bytebucket.org/Alexche77/macos_clone_c/raw/d87ddaeaccef3947eb8298a3cea655e87feb0922/images/captura.png?token=3bdcb5e3ee1a3be2086bad88327f668c7e6e28cf)

Programa escrito en C, cuyo objetivo es **simular**
_a nivel superficial y gráfico_, el funcionamiento
de aplicaciones encotradas en la versión 9 de macOS.


El software desarrollado no se considera que está 
100% optimizado pues las técnicas de desarrollo no 
son las mejores, fue un proyecto desarrollado contra el tiempo.


## Herramientas utilizadas para el desarrollo:

Esta simulación fué desarrollada en C, apoyandonos
de la librería Nuklear para el manejo de interfaces
y SDL2/OpenGL para el renderizado y manejo de buffer de video.

* Ambiente de desarrollo montado en linux:
	* Sublime text 3
	* Cmake
	* Clang compiler.
	* SDL2
	* GLEW

## Librerías externas utilizadas en este proyecto
* [Nuklear](https://github.com/vurtun/nuklear)
* [Tinydir](https://github.com/cxong/tinydir)

## Otras alternativas similares a Nuklear
* [NanoVG](https://github.com/memononen/nanovg)
* [CimGUI](https://github.com/Extrawurst/cimgui)


## Desarrolladores
* Adalberto García. (adalgarcia17@gmail.com)	
* Àlvaro Chávez Ch. (alexche7717@gmail.com) 
* Mason Ernesto Urbina G. (msn.guti5395@gmail.com)
##
 **Universidad Nacional de Ingeniería**
##
 **Managua, Nicaragua. 2017**

