/* ===============================================================
 *
 *                          FUNCIONES
 *
 * ===============================================================*/


 static void
 die(const char *fmt, ...)
 {
     va_list ap;
     va_start(ap, fmt);
     vfprintf(stderr, fmt, ap);
     va_end(ap);
     fputs("\n", stderr);
     exit(EXIT_FAILURE);
 }


static struct nk_image
cargar_imagen(const char *filename)
{
    int x,y,n;
    GLuint tex;
    unsigned char *data = stbi_load(filename, &x, &y, &n, 0);
    if (!data) die("[SDL]: No se cargo la imagen: %s", filename); else printf("[SDL]: Imagen cargada->%s\n", filename);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    stbi_image_free(data);
    return nk_image_id((int)tex);
}

//Funcion para obtener el tope de la pila
char *tope(struct stack_t *pilaDirectoriosFinder){
  if (pilaDirectoriosFinder && pilaDirectoriosFinder->tope)
    return pilaDirectoriosFinder->tope->dato;
  else
    return NULL;
}

//Funcion que copia una string
char *copiarString(char *str){
  char *tmp = malloc(strlen(str)+1);
  if (tmp)
    strcpy(tmp,str);
  return tmp;
}

//Funcion para enviar un elemento a la stack
void push(struct stack_t *pilaDirectoriosFinder, char *dato){
  struct itemStack *item = malloc(sizeof *item);
  if (item) {
    item->dato = copiarString(dato);
    // printf("[PILA]->Dato guardado con exito en la pila->%s\n", item->dato);
    item->siguiente = pilaDirectoriosFinder->tope;
    pilaDirectoriosFinder->tope = item;
    pilaDirectoriosFinder->tamStack++;
    // printf("[PILA]->Tamanho:%zu\n", pilaDirectoriosFinder->tamStack);
    // printf("[PILA]->Elemento enviado correctamente a la stack-> %s\n", tope(pilaDirectoriosFinder));
  }else{
    printf("[ERROR]->No pudimos meter el dato a la stack\n");
  }
}


//Funcion para sacar el ultimo Elemento
void pop(struct stack_t *pilaDirectoriosFinder){
  if (pilaDirectoriosFinder->tope !=NULL) {
    struct itemStack *tmp = pilaDirectoriosFinder->tope;
    pilaDirectoriosFinder->tope = pilaDirectoriosFinder->tope->siguiente;
    free(tmp->dato);
    free(tmp);
    pilaDirectoriosFinder->tamStack--;
  }
}

//Funcion para eliminar los elementos de la stack
void limpiarStack(struct stack_t *pilaDirectoriosFinder){
  while (pilaDirectoriosFinder->tope !=NULL) {
    pop(pilaDirectoriosFinder);
  }
}

void destructorPila(struct stack_t **pilaDirectoriosFinder){
  limpiarStack(*pilaDirectoriosFinder);
  free(*pilaDirectoriosFinder);
  *pilaDirectoriosFinder = NULL;
}


//Funcion que crea una nueva stack_t
struct stack_t *nuevaStack(){
  struct stack_t *pila = malloc(sizeof *pila);
  if (pila) {
    // printf("Creada la pila\n");
    pila->tope = NULL;
    pila->tamStack = 0;
  }else{
    printf("No se creo la pila!\n");
  }
  return pila;
}
