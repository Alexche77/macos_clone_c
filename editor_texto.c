
/*Esta función se llama cada vez que se requiera limpiar el texbox*/
void limpiaBufferBox(char *box_buffer);

static int
editor_texto(struct nk_context *ctx, char ptrNombreArchivo[PATH_MAX],struct iconos *iconos,   char directorioEditor[PATH_MAX], char ultimodirectorio[256], int *puedeGuardar){
    /*
    Para que las opciones de minimizar y cerrar aparezcan en la ezquina superior izquierda
    de la ventana
    */
    static enum nk_style_header_align header_align = NK_HEADER_LEFT;
    ctx->style.window.header.align = header_align;  
    /*
    Experimetnal

    Aca recibimos el nombre /ruta del archivo a abrirse
    
    if(ptrNombreArchivo != NULL){
        printf("Abriendo el archivo %s", ptrNombreArchivo);
    } 
    */

    /*Ventana principal del editor de textos*/
    if (nk_begin(ctx, "Editor de texto", nk_rect(300, 50, 400, 400), NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
    NK_WINDOW_CLOSABLE|NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
    {

        /*Variables para manejar el texto del textbox del editor*/
        static char box_buffer[512]; //Buffer principal del textbox
        /*Buffer temporal para guardar lo que está en el textbox
        al momento de intentar abrir algo y luego cancelar la acción*/
        static char box_buffer_anterior[512];
        static int box_len; //Para obtener el tamaño de los buffer

        /*Estas flags ayudana las validaciones para el buffer temporal*/
        static int box_buffer_editando;
        static int flag_editando = 0;

        /*Variables para manejar el texto del textedit al momento de guardar un archivo*/
        static char textArchive[9][20]; //Esto maneja el texto del textedit

        static char nombreArchivo[30];
        static char extensionArchivo[30];

        static int text_lenArchive[9]; //Para obtener el tamaño del texto del textedit

        /*Estas flags manejan las modales*/
        static int popup_active, popup_active_browser;

        //nk_flags active;

        /*Se inicia un menubar para agregar las opciones de "Archivo"
          A este menubar se le agregaran las opciones:
          Abrir
          Guardar
          Cerrar
        */
        nk_menubar_begin(ctx);
        nk_layout_row_begin(ctx, NK_STATIC, 25, 2);
        nk_layout_row_push(ctx, 45);

        if (nk_menu_begin_label(ctx, "Archivo", NK_TEXT_LEFT, nk_vec2(120, 200))) {
            nk_layout_row_dynamic(ctx, 30, 1);
            if (nk_menu_item_label(ctx, "Abrir", NK_TEXT_LEFT)){
                //fprintf(stdout, "Abrir\n");

                /*Activa la popup para abrir un archivo*/
                popup_active_browser = 1;

                /*
                Si hay algo editandose (hay texto) en el box del
                editor entonces el box_buffer_editando deberia ser
                mayor a 0.
                Para ello se obtiene el tamaño de buffer actual
                */
                box_buffer_editando = strlen(box_buffer);

                /*Esta flag ayuda a si se esta editando o no*/
                flag_editando = 1;

                /*Si hay algo editandose*/
                if (box_buffer_editando > 0 && flag_editando == 1){

                    /*
                    Al momento de abrir la popup entonces se dice que ya no esta
                    editando nada
                    */
                    flag_editando = 0;

                    /*
                    Entonces al momento de abrir la popup se guarda en un buffer
                    temporal lo que está actualmente en el textbox.
                    Esto se hace debido a que al momento de seleccionar un archivo
                    a abrir entonces se muestra de preview en el textbox, pero
                    si da Cancelar, entonces el textbox debe quedar lo que se estaba
                    editando antes de intentar abrir un archivo
                    */
                    strcpy(box_buffer_anterior, box_buffer);

                    /*limpiaBufferBox(box_buffer);*/
                }

                printf("Tamaño del buffer del editor: %i\n", box_buffer_editando);


            }
            if (nk_menu_item_label(ctx, "Guardar", NK_TEXT_LEFT)){

                /*Activa la popup para guardar un archivo*/
                popup_active = 1;

                /*
                Si hay algo editandose (hay texto) en el box del
                editor entonces el box_buffer_editando deberia ser
                mayor a 0.
                Para ello se obtiene el tamaño de buffer actual
                */
                box_buffer_editando = strlen(box_buffer);

                /*Esta flag ayuda a si se esta editando o no*/
                flag_editando = 1;

                /*Si hay algo editandose*/
                if (box_buffer_editando > 0 && flag_editando == 1){

                    /*
                    Al momento de abrir la popup entonces se dice que ya no esta
                    editando nada
                    */
                    flag_editando = 0;

                    /*
                    Entonces al momento de abrir la popup se guarda en un buffer
                    temporal lo que está actualmente en el textbox.
                    Si da Cancelar, entonces el textbox debe quedar lo que se estaba
                    editando antes de intentar guardar un archivo
                    */
                    strcpy(box_buffer_anterior, box_buffer);
                }

            }
            if (nk_menu_item_label(ctx, "Salir", NK_TEXT_LEFT)){
                fprintf(stdout, "Salir\n");
            }

            nk_menu_end(ctx);
        }


        /*Textbox para el editor*/
        float alto = nk_window_get_height(ctx)-100;      
        nk_layout_row_dynamic(ctx, (int)alto, 1);
        // nk_layout_row_dynamic(ctx, 300, 1);
        nk_edit_string(ctx, NK_EDIT_BOX, box_buffer, &box_len, 512, nk_filter_ascii);
        //nk_edit_string(ctx, NK_EDIT_BOX, text[7], &text_len[7], 512, nk_filter_default);

        /*Si la el popup de Abrir archivo está activa*/
        if (popup_active_browser){

            /*Tamaño del group de arhivos, es donde se listan los archivos a Abrir*/
            static int group_width = 310;
            static int group_height = 170;

            /*Tamañano de la popup de Abrir archivos*/
            static struct nk_rect s = {20, 80, 340, 260};
            if (nk_popup_begin(ctx, NK_POPUP_STATIC|NK_WINDOW_MOVABLE, "Abrir", 0, s)){
                nk_layout_row_dynamic(ctx, 25, 1);
                nk_label(ctx, "Abrir archivo...", NK_TEXT_LEFT);

                /*Layout que contendrá al group donde se mostrarán archivos abrir*/
                nk_layout_row_static(ctx, (float)group_height, group_width, 2);
                if (nk_group_begin(ctx, "Lista de archivos", NK_WINDOW_BORDER|NK_WINDOW_TITLE)) {

                    /*
                    Manejo de directorioEditor para abrir los archivos
                    En este caso, se buscan los archivos en la raíz donde
                    se ejecute este programa
                    */
                    DIR *path;
                    struct dirent *pp;

                    path = opendir (directorioEditor);

                    // printf("Abrimos el directorio %s\n", directorioEditor);

                    /*Si se encuentra el directorioEditor*/
                    if (path != NULL){
                        /*
                        Se lee todo lo que hay en el directorioEditor y se guarda en la estruct
                        dirent *pp para accededer al nombre de los archivos
                        */
                        nk_layout_row_static(ctx, 22, 150, 1);
                        while ((pp = readdir (path))!=NULL) {

                          //Se obtiene la longitud del nombre de los archivos
                          int length = strlen(pp->d_name);

                          /*
                          Si las últimas cuatro letras del nombre del archivo
                          cumple con .txt entonces solo esos archivos se mostrarán.
                          Esto es para que solo se puedan mostrar y abrir los archivos .txt
                          */
                          if (strncmp(pp->d_name + length - 4, ".txt", 4) == 0) {

                            /*
                            Se pinta cada uno de los archivos con su correspondiente
                            nombre pp->d_name
                            */

                            if (nk_button_label(ctx, pp->d_name)) {

                                printf("Box buffer anterior: \n%s\n", box_buffer_anterior);

                                /*
                                Se limpia el buffer del textbox para que cada vez
                                que se seleccione un archivo, se muestre como preview
                                antes de Aceptar para abrir
                                */
                                limpiaBufferBox(box_buffer);

                                /*Puntero para abrir el archivo seleccionado*/
                                FILE *ptr_file;
                                char buf[512] = "";

                                /*
                                Se abre el archivo seleccionado de acuerdo al
                                nombre que tenga pp->d_name
                                */
                                //Tenemos que concatenar el directorioEditor al nombre del archivo
                                char archivoConRuta[100]="";
                                strcat(archivoConRuta,directorioEditor);
                                strcat(archivoConRuta,"/");
                                strcat(archivoConRuta,pp->d_name);
                                printf("Queremos abrir el archivo %s\n", archivoConRuta);
                                ptr_file = fopen(archivoConRuta,"r");
                                if (!ptr_file){
                                  printf("No pudimos abrir el archivo en %s\n", archivoConRuta);
                                  return 1;
                                }else{
                                  /*
                                  Se guarda en la variable buf todo lo que se lea
                                  del archivo seleccionado
                                  */
                                  while (fgets(buf,512, ptr_file)!=NULL){

                                      /*
                                      Todo lo que se lea del archivo seleccionado
                                      se concatena al box_buffer que es limpiado antes
                                      pintar todo en él
                                      */
                                      strcat(box_buffer, buf);

                                  }

                                  /*
                                  Este cálculo de la longitud actual del box_buffer
                                  se hace para que pueda pintarse todo en el box_buffer
                                  debido a que tiene que saber cuánto es lo que se va a
                                  pintar en él
                                  */
                                  box_len = strlen(box_buffer);

                                  /*Cierre del archivo*/
                                  fclose(ptr_file);
                                }

                            }

                          }else{
                            //El archivo encontrado es un directorioEditor, esto nos permite navegar en el
                            if (nk_button_image_label(ctx,iconos->carpetaIcon,pp->d_name,NK_TEXT_LEFT)){
                              if (strcmp(pp->d_name,"..")==0 || strcmp(pp->d_name,".") ==0 ) {

                                //Vamos a bloquear el acceso al directorioEditor mas arriba de HOME del SO
                                //Si el directorioEditor actual es home, entonces lo seteamos de nuevo a home
                                if (strcmp(directorioEditor,HOME_DIR)) {
                                  printf("El acceso esta bloqueado\n");
                                  strcpy(directorioEditor,HOME_DIR);
                                }else{
                                  printf("Navegando hacia atras %s\n", ultimodirectorio);
                              
                                }

                              }else{
                                printf("No es el punto\n");
                                strcpy(ultimodirectorio, pp->d_name);
                                printf("Ultimo directorioEditor ahora es-> %s\n", ultimodirectorio);
                                strcat(directorioEditor,"/");
                                strcat(directorioEditor,pp->d_name);
                                printf("Cambiando a carpeta-> %s\n", directorioEditor);
                              }
                            }
                          }

                        }
                        /*Cierre del directorioEditor*/
                        (void) closedir (path);

                    }

                    /*Cierrre del gropu que contiene a la lista de archivos*/
                    nk_group_end(ctx);

                }

                /*Botón Aceptar al momento de abrir un archivo*/
                nk_layout_row_begin(ctx, NK_STATIC, 30, 3);
                nk_layout_row_push(ctx, 60);
                if (nk_button_label(ctx, "Aceptar")) {

                    /*
                    Como cuando el archivo se selecciona para abrir, ya se pinta
                    todo el texto de él en el texbox, entonces cuando se presione
                    aceptar solo se cierra el popup
                    */
                    popup_active_browser = 0;
                    nk_popup_close(ctx);
                }
                nk_layout_row_push(ctx, 60);
                if (nk_button_label(ctx, "Cancelar")) {

                    /*************************************************
                        HAY UN "BUG": CUANDO HAY TEXTO EN EL
                        BOX DEL EDITOR Y SE BORRA, SE SUPONE
                        QUE QUEDA VACIO EL BOX, PERO AL MOMENTO
                        DE ABRIR Y CANCELAR QUEDA EL TEXTO "BORRADO"
                        EN ESE BOX, LO TENGO ARREGLADO PERO NO ME
                        GUSTA COMO QUEDA
                    **************************************************/

                    /*
                    Si está editando (hay texto en el texbox)
                    entonces al momento de seleccionar Cancelar
                    cuando se está abriendo un archivo, el box_buffer
                    se limpia y se vuelve a pintar lo que estaba
                    en el texbox antes de intentar abrir un archivo
                    */
                    if (box_buffer_editando > 0) {
                        memset(box_buffer, 0, 512);
                        strcat(box_buffer, "");
                        box_len = strlen(box_buffer);

                        strcat(box_buffer, box_buffer_anterior);
                        box_len = strlen(box_buffer);
                    }
                    /*
                    Si no se está editando, y se intenta abrir un archivo,
                    como al momento de seleccionar el archivo a abrir se ve
                    un preview, entonces el box_buffer se llena, pero como
                    en este punto ha dado Cancelar, el textbox queda limpio
                    a como estaba, con solo limpiar el box_buffer
                    */
                    if (box_buffer_editando < 1){
                        memset(box_buffer, 0, 512);
                        strcat(box_buffer, "");
                        box_len = strlen(box_buffer);
                    }

                    /*Cierre de la popup*/
                    popup_active_browser = 0;
                    nk_popup_close(ctx);
                }
                /*Cierre del layout donde se pintan los elementos de la popup*/
                nk_layout_row_end(ctx);

                /*Cierre de la popup*/
                nk_popup_end(ctx);
            } else popup_active_browser = nk_false;
        }

        /*Si la el popup de Guardar archivo está activa*/
        if (popup_active){

            /*Tamañano de la popup de Guardar archivos*/
            static struct nk_rect s = {20, 100, 400, 350};
            if (nk_popup_begin(ctx, NK_POPUP_STATIC, "Guardar", 0, s)){
                nk_layout_row_dynamic(ctx, 25, 1);
                nk_label(ctx, "Guardar archivo..", NK_TEXT_LEFT);
                
                /*Texto al lado izquierdo del textedit*/
                nk_layout_row_begin(ctx, NK_STATIC, 30, 4);
                nk_layout_row_push(ctx, 100);
                nk_label(ctx, "Nombre:", NK_TEXT_LEFT);             

                /*Textedit para el nombre del archivo*/
                nk_layout_row_push(ctx, 150);
                nk_edit_string(ctx, NK_EDIT_FIELD, textArchive[7], &text_lenArchive[7], 20, nk_filter_default);                                
                nk_layout_row_end(ctx);
                //Primero calculamos si esta vacia la caja de texto                                
                if(text_lenArchive[7]>1){                    
                    *puedeGuardar = nk_true;
                }else{
                    *puedeGuardar = nk_false;
                    
                }
                if(*puedeGuardar==nk_false){
                    nk_layout_row_dynamic(ctx, 25,1);
                    nk_label_colored(ctx,"No puedes guardar un archivo con un nombre tan corto",NK_TEXT_ALIGN_CENTERED,nk_rgb(255,0,0));                    
                }
                  
                /*Logica donde se muestra el navegador de archivos donde se va a guardar el archivo*/                 
                nk_layout_row_static(ctx, 200,360, 1);
                if (nk_group_begin(ctx, "¿Donde deseas guardarlo?", NK_WINDOW_BORDER|NK_WINDOW_TITLE)){
                      /*
                    Manejo de directorioEditor para abrir los archivos
                    En este caso, se buscan los archivos en la raíz donde
                    se ejecute este programa
                    */
                    DIR *path;
                    struct dirent *pp;

                    path = opendir (directorioEditor);

                    // printf("Abrimos el directorio %s\n", directorioEditor);

                    /*Si se encuentra el directorioEditor*/
                    if (path != NULL){
                        nk_layout_row_static(ctx, 22, 150, 1);
                        while ((pp = readdir (path))!=NULL) {

                          //Se obtiene la longitud del nombre de los archivos
                          int length = strlen(pp->d_name);

                          /*
                          Si las últimas cuatro letras del nombre del archivo
                          cumple con .txt entonces solo esos archivos se mostrarán.
                          Esto es para que solo se puedan mostrar y abrir los archivos .txt
                          */
                          if (strncmp(pp->d_name + length - 4, ".txt", 4) == 0) {
                            nk_button_image_label(ctx,iconos->textIcon,pp->d_name,NK_TEXT_LEFT);
                          }else{
                            //El archivo encontrado es un directorioEditor, esto nos permite navegar en el
                            if (nk_button_image_label(ctx,iconos->carpetaIcon,pp->d_name,NK_TEXT_LEFT)){
                              if (strcmp(pp->d_name,"..")==0 || strcmp(pp->d_name,".") ==0 ) {

                                //Vamos a bloquear el acceso al directorioEditor mas arriba de HOME del SO
                                //Si el directorioEditor actual es home, entonces lo seteamos de nuevo a home
                                if (strcmp(directorioEditor,HOME_DIR)) {
                                  printf("El acceso esta bloqueado\n");
                                  strcpy(directorioEditor,HOME_DIR);
                                }else{
                                  printf("Navegando hacia atras %s\n", ultimodirectorio);
                              
                                }

                              }else{
                                printf("No es el punto\n");
                                strcpy(ultimodirectorio, pp->d_name);
                                printf("Ultimo directorioEditor ahora es-> %s\n", ultimodirectorio);
                                strcat(directorioEditor,"/");
                                strcat(directorioEditor,pp->d_name);
                                printf("Cambiando a carpeta-> %s\n", directorioEditor);
                              }
                            }
                          }
                        }
                    }

                    /*Cierre del directorioEditor*/
                    (void) closedir (path);
                }
                nk_group_end(ctx);
                
                nk_layout_row_dynamic(ctx, 25, 2);                
                if (nk_button_label(ctx, "Guardar")) {
                   if(*puedeGuardar){
                    printf("Guardado es permitido");
                    /*
                    Nombre del archivo

                    Se obtiene el nombre del archivo
                    */
                    //textArchive[7][text_lenArchive[7]];
                    text_lenArchive[7]++;

                    /*Se limpian las variables cada vez que se guarda un archivo*/
                    strcpy(nombreArchivo, "");
                    strcpy(extensionArchivo, "");

                    /*A la variable de nombreArchivo se copia lo que contiene el textedit*/
                    strcpy(nombreArchivo, textArchive[7]);

                    /*Al la variable de extensionArchivo se copia la extensión*/
                    strcpy(extensionArchivo, ".txt");

                    /*AL nombre del archivo se le concatena la extensión*/
                    strcat(nombreArchivo, extensionArchivo);
                    printf("Nombre del archivo: %s\n", nombreArchivo);

                    /*Se limpia el textedit después de obtener el texto*/
                    text_lenArchive[7] = 0;
                    strcpy(textArchive[7], "");

                    memset(textArchive[7], 0, 20);
                    strcat(textArchive[7], "");
                    text_lenArchive[7] = strlen(textArchive[7]);

                    // strcpy(nombreArchivo, "");
                    // strcpy(extensionArchivo, "");

                    /*Texto del editor*/
                    //box_buffer = '\n';
                    //text_len[7]++;

                    printf("Texto del editor: %s\n", box_buffer_anterior);
                    char dirDocs[PATH_MAX]="";
                    strcpy(dirDocs,directorioEditor);
                    printf("La variable directorioEditor tiene= %s ", directorioEditor);
                    printf("Guardando texto en %s \n",dirDocs);    
                    strcat(dirDocs,"/");
                    strcat(dirDocs,nombreArchivo);
                    /*Se abre el archivo creado*/
                    FILE *f = fopen(dirDocs, "w+");

                    if (f == NULL){
                        printf("Error abriendo el archivo\n");
                        exit(1);
                    }

                    //fprintf(f, "Texto del editor:\n\n%s\n", box_buffer_anterior);
                    /*
                    Al archivo creado se le asigna el box_buffer_anterior debido a que
                    puede que se haya abierto un archivo anteriormente y luego modificado
                    y luego intentar guardarlo
                    */
                    fprintf(f, "%s", box_buffer_anterior);
                    printf("Guardando texto: %s en %s \n", box_buffer_anterior, dirDocs);                    
                    fclose(f);

                    /*Ciere de la popup*/
                    popup_active = 0;
                    nk_popup_close(ctx);
                   }
                }
                /*Si se presiona el botón Cancelar*/
                if (nk_button_label(ctx, "Cancelar")) {

                    /*... Solo se cierra la popup*/
                    popup_active = 0;
                    nk_popup_close(ctx);
                }
                nk_popup_end(ctx);
            } else popup_active = nk_false;
        }

    }
    /*Cierre de la ventana principal del editor*/
    nk_end(ctx);
    return !nk_window_is_hidden(ctx, "Editor de texto");
}

/*Esta función se llama cada vez que se requiera limpiar el texbox*/
void limpiaBufferBox(char *box_buffer){

    static int box_len; //Para obtener el tamaño del buffer

    memset(box_buffer, 0, 512);
    strcat(box_buffer, "");
    box_len = strlen(box_buffer);
}
