
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include <time.h>

#include <sys/types.h>
#include <dirent.h>
#include <linux/limits.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>



// Librerias necesarias para finder
#define _UNICODE
#define UNICODE
#include "include/tinydir.h"
#include <sys/stat.h>
#include <unistd.h>
#define HOME_DIR "./home"
#define DOCS_DIR "documentos"

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#define NK_SDL_GL3_IMPLEMENTATION
#include "include/nuklear.h"
#include "include/nuklear_sdl_gl3.h"

#define STB_IMAGE_IMPLEMENTATION
#include "include/stb_image.h"

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 576

#define MAX_VERTEX_MEMORY 512 * 1024
#define MAX_ELEMENT_MEMORY 128 * 1024

#define UNUSED(a) (void)a
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define LEN(a) (sizeof(a)/sizeof(a)[0])

/*
*
*                 ESTRUCTURAS DE DATOS
*
*
*/
/*    Estructuras de datos necesarias en el programa  */

struct iconos{
  struct nk_image carpetaIcon;
  struct nk_image textIcon;
  struct nk_image atrasFlecha;
  struct nk_image homeIcono;
  struct nk_image nuevaCarpeta;
};

//Esta estructura es un conjunto de imagenes que cargamos y utilizamos
//en todo en todo el programa.
struct media{

      struct nk_image apple_logo;
      struct nk_image fondo;
      struct nk_image calculadora;
      struct nk_image editor_texto;
      struct nk_image finder;
      struct iconos iconos;

};


struct programas{
  int calculadora;
  int editor;
  int finder;
  int about;
};


//Estructuras de datos necesarias para el algoritmo de pila de cadenas
struct itemStack{
  char *dato;
  struct itemStack *siguiente;
};

struct stack_t{
  struct itemStack *tope;
  size_t tamStack;
};
/* ===============================================================
 *
 *                          Librerias
 *
 * ===============================================================*/
#include "utilidades.c"
#include "style.c"
#include "barra_superior.c"
#include "calculator.c"
#include "editor_texto.c"
#include "finder.c"
#include "about.c"



/* ===============================================================
 *
 *                          MAIN
 *
 * ===============================================================*/
int
main(int argc, char* argv[])
{
    /* Declaracion de la plataforma en la que va a correr nuklear
       En este caso escogimos SDL2
     */
     //Creamos una variable de ventana SDL.
    SDL_Window *win;
    SDL_GLContext glContext;    //Contexto SDL para OpenGL
    struct nk_color fondo;      //Variable para guardar el color de fondo
    int ancho_ventana, alto_ventana;
    int corriendo = 1;
    int mostrandoPopup = 0;
    int *ptrmostrandoPopup = &mostrandoPopup;     

    //Variables

    glEnable(GL_TEXTURE_2D);

    struct media media;
    struct nk_context *ctx;
    struct programas programas;
    struct stack_t *pilaDirectoriosFinder = nuevaStack();

    /* Setup para SDL */
    SDL_SetHint(SDL_HINT_VIDEO_HIGHDPI_DISABLED, "0");
    SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER|SDL_INIT_EVENTS);
    SDL_GL_SetAttribute (SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute (SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    win = SDL_CreateWindow("macOS",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN|SDL_WINDOW_ALLOW_HIGHDPI);
    glContext = SDL_GL_CreateContext(win);
    SDL_GetWindowSize(win, &ancho_ventana, &alto_ventana);

    /* Setup para OpenGL */
    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    glewExperimental = 1;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "----No pudimos inicializar GLEW\n");
        exit(1);
    }


    ctx = nk_sdl_init(win);
    /* Cargamos las fuentes a utilizarse en el programa */
    {struct nk_font_atlas *atlas;
    nk_sdl_font_stash_begin(&atlas);
    // struct nk_font *roboto = nk_font_atlas_add_from_file(atlas, "extra_fuente/Roboto-Regular.ttf", 16, 0);
    struct nk_font *chicago = nk_font_atlas_add_from_file(atlas, "extra_fuente/Chicago.ttf", 14, 0);
    nk_sdl_font_stash_end();
    nk_style_set_font(ctx, &chicago->handle);}



    //Cargando imagenes
    media.apple_logo = cargar_imagen("./images/logo.png");
    media.calculadora = cargar_imagen("./images/calculadora.png");
    media.editor_texto = cargar_imagen("./images/notepad.png");
    media.finder = cargar_imagen("./images/finder.png");
    //Cargando iconos
    media.iconos.textIcon = cargar_imagen("./icon/text.png");
    media.iconos.carpetaIcon = cargar_imagen("./icon/directory.png");
    media.iconos.atrasFlecha = cargar_imagen("./icon/prev.png");
    media.iconos.homeIcono = cargar_imagen("./icon/home.png");
    media.iconos.nuevaCarpeta = cargar_imagen("./icon/newfolder.png");
    printf("FASE DE CARGA DE IMAGENES **LISTA**\n");



    //Aplicamos el tema general
    //Cualquier cambio que se desee hacer al tema debera hacerse en styles.c
    //o despues de este metodo para que se apliquen
    set_style(ctx, THEME_WHITE);


    /*Programas cerrados por defecto*/
    programas.calculadora = nk_false;
    programas.editor = nk_false;
    programas.finder = nk_false;
    programas.about = nk_false;

    fondo = nk_rgb(122,123,190);

    //Tinydir nos permite movernos dentro de la estructura de archivos del sistema
    //a emular
    tinydir_dir dir;
    char directorioEditor[PATH_MAX];
    char directorioFinder[PATH_MAX];
    char ultimoDirectorio[256];
    strcpy(ultimoDirectorio,"home");

    /* INICIO:  Logica de creación de directorio del sistema   */
    /****El siguiente fragmento de codigo necesita optimizacion****/
    //En esta fase buscamos si en el directorio del ejecutable
    //tenemos la carpeta home, si no existe procedemos a crearla
    //con mkdir.    (Unicamente funciona en linux)
    strcat(directorioFinder, HOME_DIR);
    if(tinydir_open(&dir, directorioFinder) == -1){
      //Creando el directorio home
      int resultado =mkdir(HOME_DIR, 0777);
      printf("Creacion del directorio resulto en: %d\n", resultado);
      if (resultado == 0) {
        printf("Creado el directorio HOME\n");
        printf("Directorio cargado\n");
        char docsDir[100] = HOME_DIR;
        strcat(docsDir,"/");
        strcat(docsDir, DOCS_DIR);
        int resultado = mkdir(docsDir, 0777);
        if (resultado) {
          printf("Directorio %s  CREADO\n", docsDir);
          strcpy(directorioEditor,docsDir);
        }
      }else{
        printf("Fallo al crear el directorio HOME\n");
      }
    }else{
      printf("Directorio cargado\n");
      char docsDir[100] = HOME_DIR;
      strcat(docsDir,"/");
      strcat(docsDir, DOCS_DIR);
      int resultado = mkdir(docsDir, 0777);
      if (resultado) {
        printf("Directorio %s  CREADO\n", docsDir);
        strcpy(directorioEditor,docsDir);
      }
    }
    //Cerramos el directorio que abrimos previamente
    tinydir_close(&dir);
    //Mandamos a la pila el directorio home, porque se va a cargar
    //apenas abramos la interfaz
    push(pilaDirectoriosFinder,"home");
    /* FIN:  Logica de creación de directorio del sistema   */


    printf("En la pila, el elemento al tope es-> %s\n", tope(pilaDirectoriosFinder));
    /*CICLO principal*/

    int resetCalculadora = nk_true;   //Variable que controla el reseteo de la calculadora al cerrarla.
    int puedeGuardar = nk_false;      //Controladora de validacion al guardar un archivo sin nombre
    //----EXPERIMENTAL
    /*
    La lógica a implementar consiste en enviarle a nuklear un nombre dinámico, 
    dependiente de la ruta del archivo, para que vaya creando en memoria instancias nuevas
    de editores de texto, lo que nos permitiría cargar desde el Finder archivos de 
    texto deseados.
    */
    char nombreArchivo[PATH_MAX] = "Editor de texto"; //Nombre por defecto que tendria la ventana de editor de texto




    /*
    IMPORTANTE:

    La lógica de este programa se desarrolla bajo el principio de ejecución de un ciclo infinito,
    esto permite la actualización inmediata de la interfaz grafica. 
    
    */  
    while (corriendo)
    {
        /* Recepcion de eventos de input a la ventana, esto lo maneja sdl */
        SDL_Event evt;
        /* Inputs como el teclado y raton en la interfaz de nuklear lo maneja nuklear*/
        nk_input_begin(ctx);
        while (SDL_PollEvent(&evt)) {
          //Este if controla el cierre de la ventana, nada mas
            if (evt.type == SDL_QUIT) goto salir;
            nk_sdl_handle_event(&evt);
        }
        nk_input_end(ctx);


        /* GUI */ 
        /*
        El controlador de ventanas a mostrar
        se basa en lógica de booleanas,
        se almacena en una estructura simulando de esta manera
        una tabla de los programas "instalados" en el SO.

        Se le envía la dirección de la estructura a cada función
        puesto que tiene que modificar su propia booleana al cerrarse.      

        Básicamente, si su booleana esta en true, entonces el programa se muestra.

        */

        /*----------------Dock superior---------*/
        barra_superior(ctx, &media, &programas);
        /* -------------- Programas ---------------- */

        if (programas.calculadora) {
           programas.calculadora = calculator(ctx,&resetCalculadora);
          resetCalculadora = nk_false;
        }else{

          resetCalculadora = nk_true;
        }
        if (programas.editor) {
          programas.editor = editor_texto(ctx,nombreArchivo,&media.iconos, directorioEditor, ultimoDirectorio, &puedeGuardar);
        }
        if (programas.finder) {
          programas.finder = finder(ctx, &dir, &media.iconos, directorioFinder, directorioEditor ,pilaDirectoriosFinder, ptrmostrandoPopup, &programas, nombreArchivo);
        }

        if (programas.about) {
          programas.about = about(ctx,&media);
        }


        /* ----------------------------------------- */

        /* 
        Dibujamos el fondo y limpiamos toda la pantalla para redibujar,
        sdl y opengl se encargan de esto.
        */

        {
        float bg[4];
        nk_color_fv(bg, fondo);
        SDL_GetWindowSize(win, &ancho_ventana, &alto_ventana);
        glViewport(0, 0, ancho_ventana, alto_ventana);
        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(bg[0], bg[1], bg[2], bg[3]);
        nk_sdl_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_MEMORY, MAX_ELEMENT_MEMORY);
        SDL_GL_SwapWindow(win);}

    }

salir:
    nk_sdl_shutdown();
    tinydir_close(&dir);
    destructorPila(&pilaDirectoriosFinder);
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(win);
    SDL_Quit();
    return 0;
}
