
static int
about(struct nk_context *ctx, struct media *media)
{
    if (nk_begin(ctx, "Información del sistema", nk_rect((WINDOW_WIDTH/2)-(420/2), (WINDOW_HEIGHT/2)-(500/2), 420, 400),
    NK_WINDOW_BORDER|NK_WINDOW_CLOSABLE|NK_WINDOW_TITLE | NK_WINDOW_NO_SCROLLBAR))
    {
    	nk_layout_row_begin(ctx, NK_STATIC, 200, 3);
        nk_layout_row_push(ctx, 100);  
        nk_label(ctx,"",NK_TEXT_ALIGN_CENTERED);
        nk_layout_row_push(ctx, 200);
        nk_button_image(ctx, media->finder);        
        nk_layout_row_push(ctx, 100);        
        nk_label(ctx,"",NK_TEXT_ALIGN_CENTERED);
        nk_layout_row_end(ctx);        
        nk_layout_row_static(ctx, 15,400, 1);
        nk_label(ctx,"macOS 9", NK_TEXT_ALIGN_CENTERED);
        nk_label(ctx,"Desarrollado por:", NK_TEXT_ALIGN_LEFT);
        nk_label(ctx,"-Adalberto García", NK_TEXT_ALIGN_CENTERED);
        nk_label(ctx,"-Alvaro Chévez", NK_TEXT_ALIGN_CENTERED);
        nk_label(ctx,"-Mason Urbina", NK_TEXT_ALIGN_CENTERED);
    }
    nk_end(ctx);
    return !nk_window_is_hidden(ctx, "Información del sistema");
}
